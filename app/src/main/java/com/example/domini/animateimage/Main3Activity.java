package com.example.domini.animateimage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class Main3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        setImageListener();
    }

    private void setImageListener() {
        ImageView image = (ImageView) findViewById(R.id.marker_image);

        image.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Toast.makeText(Main3Activity.this, "ACTION DOWN", Toast.LENGTH_SHORT).show();
                        return true;
                    case MotionEvent.ACTION_UP:
                        Toast.makeText(Main3Activity.this, "ACTION UP", Toast.LENGTH_SHORT).show();
                        return true;
                }

                return false;
            }
        });
    }
}
