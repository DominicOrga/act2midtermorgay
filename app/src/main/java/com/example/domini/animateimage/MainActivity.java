package com.example.domini.animateimage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView imageA = (ImageView) findViewById(R.id.image_a);
        ImageView imageB = (ImageView) findViewById(R.id.image_b);

        fadeImage(imageA, imageB);
        startActivity2(2000);
    }

    private void fadeImage(ImageView a, ImageView b) {
        a.animate()
                .rotationBy(3600)
                .alpha(0f)
                .setDuration(1000)
                .start();

        b.animate()
                .rotationBy(3600)
                .alpha(1f)
                .setDuration(1000);
    }

    private void startActivity2(final int delay) {
        Thread thread = new Thread() {
            public void run() {
                try {
                    sleep(delay);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                    startActivity(intent);
                    finish();
                }

            }
        };

        thread.start();
    }
}
